# Exchanger


Simple currency exchanger with mocking API based Open Exchange Rates data format.

## Runnig

Docker and Docker Compose are required to run the project. First build required images by issuing  command

```make build```

After building process project is ready to be run with command

```make up```

## Frontend Application
Application is written in React with Redux store and test written in Jest. Babel is used for transpilation and ESLint for linting. For building Parcel is used instead of Webpack for faster build times and ease of implementation.

### Features
  - Pockets widget with balance
  - Exchange widget
  - History widget with list of operations

## Servies
All serivces are written in Node using Express.

### API
Because Open Exchange Rates free plan offers only 1000 request per month and rates are changing only once per 24 hours, service is not best suited for development. Mocking API microservice provides Open Exchange Rates format data with changing values on each request. Service by default is listening on port `3002` and aswers on `/api/latest.json` endpoint.

`http://localhost:3002/api/latest.json`

### Emitter
Changing data provides great oportunity to push rather than pull data from the frondend part. Emitter service is checking API every 10 seconds and pushing the results using Server Sent Events.  By default is listening on port `3001` and answeres on `/push/latest` endpoint.

Emitter provides configuration based on environment variables, so can be used both with mocking API and Open Exchange Rates.

`htto://localhost:3001/push/latest`

### UI
Simple Express based service to host the frontend bundle and provide configuration for the React application. Uses port `3000`.