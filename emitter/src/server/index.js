import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import EventEmitter from 'events';
import worker from './worker';
import push from './routes/push/push';
import { APP_PORT } from './constants';

const app = express();
const emitter = new EventEmitter();

worker({ emitter });

app.use([
  morgan('dev'),
  cors({
    origin: ['http://localhost:3000'],
  }),
  push({ emitter }),
]);

app.listen(APP_PORT || 3000, () => {
  // eslint-disable-next-line no-console
  console.log(`Listening on port ${APP_PORT}.`);
});
