/* eslint-disable import/prefer-default-export */

export const {
  APP_PORT = 3000,
  API_BASEPATH,
  API_KEY,
  EVENT_NAME = 'push',
} = process.env;
