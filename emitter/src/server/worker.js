import axios from 'axios';
import {
  API_BASEPATH,
  API_KEY,
  EVENT_NAME,
} from './constants';

const worker = ({ emitter }) => setInterval(() => {
  axios
    .get(API_BASEPATH, {
      params: {
        app_id: API_KEY,
      },
    })
    .then(({ data }) => {
      emitter.emit(EVENT_NAME, 'rates', data);
    }).catch(() => {
      emitter.emit(EVENT_NAME, 'error', { message: 'Exchange rates API is not responding.' });
    });
}, 2000);

export default worker;
