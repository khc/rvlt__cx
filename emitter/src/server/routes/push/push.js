import { Router } from 'express';
import latest from './latest';

export default (options) => {
  const router = Router();

  router.use('/push', [
    latest(options),
  ]);

  return router;
};
