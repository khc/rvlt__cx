import { Router } from 'express';
import { EVENT_NAME } from '../../constants';

const latest = ({ emitter }) => (req, res) => {
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    Connection: 'keep-alive',
  });

  const heartbeat = setInterval(() => {
    res.write('event: heartbeat\n');
    res.write('data: ❤️\n\n');
  }, 15000);

  emitter.on(EVENT_NAME, (event, data) => {
    res.write('retry: 500\n');
    res.write(`event: ${event}\n`);
    res.write(`data: ${JSON.stringify(data)}\n\n`);
  });

  req.on('close', () => {
    clearInterval(heartbeat);
  });
};

export default (options) => {
  const router = Router();

  router.get('/latest', latest(options));

  return router;
};
