import ratesReducer from '../../../../src/client/store/reducers/ratesReducer';
import { SET_RATES } from '../../../../src/client/store/actions/ratesActions';

describe('ratesReducer', () => {
  it('should update store', () => {
    const mockAction = {
      type: SET_RATES,
      payload: {
        base: 'USD',
        rates: {
          USD: 1,
          EUR: 0.8,
        },
      },
    };
    const expectedState = {
      base: 'USD',
      rates: {
        USD: 1,
        EUR: 0.8,
      },
      loaded: true,
    };

    expect(ratesReducer({}, mockAction)).toStrictEqual(expectedState);
  });
});
