import historyReducer from '../../../../src/client/store/reducers/historyReducer';
import { ADD_HISTORY } from '../../../../src/client/store/actions/historyActions';

describe('ratesReducer', () => {
  it('should update store', () => {
    const mockMessage = 'lorem ipsum';
    const mockAction = {
      type: ADD_HISTORY,
      payload: {
        message: mockMessage,
      },
    };
    const expectedState = [{
      message: mockMessage,
    }];

    expect(historyReducer([], mockAction)).toStrictEqual(expectedState);
  });
});
