import pocketsReducer from '../../../../src/client/store/reducers/pocketsReducer';
import { ADD_POCKET, SET_BALANCE, UPDATE_BALANCE } from '../../../../src/client/store/actions/pocketsActions';

describe('ratesReducer', () => {
  it('should add pocket to the store', () => {
    const mockAction = {
      type: ADD_POCKET,
      payload: {
        currency: 'EUR',
        balance: 0,
      },
    };
    const expectedState = [{
      currency: 'EUR',
      balance: 0,
    }];

    expect(pocketsReducer([], mockAction)).toStrictEqual(expectedState);
  });
  it('should set pocket balance in store', () => {
    const mockAction = {
      type: SET_BALANCE,
      payload: {
        currency: 'EUR',
        balance: 10,
      },
    };
    const initialState = [{
      currency: 'EUR',
      balance: 0,
    }, {
      currency: 'USD',
      balance: 0,
    }];
    const expectedState = [{
      currency: 'EUR',
      balance: 10,
    }, {
      currency: 'USD',
      balance: 0,
    }];

    expect(pocketsReducer(initialState, mockAction)).toStrictEqual(expectedState);
  });
  it('should update pocket balance in store', () => {
    const mockAction = {
      type: UPDATE_BALANCE,
      payload: {
        currency: 'EUR',
        amount: -5,
      },
    };
    const initialState = [{
      currency: 'EUR',
      balance: 10,
    }, {
      currency: 'USD',
      balance: 0,
    }];
    const expectedState = [{
      currency: 'EUR',
      balance: 5,
    }, {
      currency: 'USD',
      balance: 0,
    }];

    expect(pocketsReducer(initialState, mockAction)).toStrictEqual(expectedState);
  });
});
