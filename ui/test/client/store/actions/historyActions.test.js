import { addHistory, ADD_HISTORY } from '../../../../src/client/store/actions/historyActions';

describe('historyActions', () => {
  it('should create Redux action with icon', () => {
    const mockMessage = 'lorem ipsum';
    const mockIcon = 'icon';
    const expectedAction = {
      type: ADD_HISTORY,
      payload: {
        message: mockMessage,
        icon: mockIcon,
      },
    };

    expect(addHistory(mockMessage, mockIcon)).toMatchObject(expectedAction);
  });
  it('should create Redux action without icon', () => {
    const mockMessage = 'lorem ipsum';
    const mockIcon = '';
    const expectedAction = {
      type: ADD_HISTORY,
      payload: {
        message: mockMessage,
        icon: mockIcon,
      },
    };

    expect(addHistory(mockMessage)).toMatchObject(expectedAction);
  });
});
