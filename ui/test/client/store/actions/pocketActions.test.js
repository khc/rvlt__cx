import {
  _addPocket,
  addPocket,
  ADD_POCKET,
  _setBalance,
  setBalance,
  SET_BALANCE,
  _updateBalance,
  updateBalance,
  UPDATE_BALANCE,
  exchange,
} from '../../../../src/client/store/actions/pocketsActions';
import { ADD_HISTORY } from '../../../../src/client/store/actions/historyActions';

describe('pocketActions', () => {
  let mockDispatch;

  beforeEach(() => {
    mockDispatch = jest.fn();
  });
  afterEach(() => {
    jest.restoreAllMocks();
  });
  it('should create ADD_POCKET action', () => {
    const expectedAction = {
      type: ADD_POCKET,
      payload: {
        currency: 'EUR',
        balance: 0,
      },
    };

    expect(_addPocket('EUR')).toStrictEqual(expectedAction);
    expect(_addPocket('EUR', 0)).toStrictEqual(expectedAction);
  });
  it('should dispatch ADD_POCKET with history', () => {
    addPocket('EUR')(mockDispatch);

    expect(mockDispatch.mock.calls[0][0]).toMatchObject({ type: ADD_POCKET });
    expect(mockDispatch.mock.calls[1][0]).toMatchObject({ type: ADD_HISTORY });
  });
  it('should create SET_BALANCE action', () => {
    const expectedAction = {
      type: SET_BALANCE,
      payload: {
        currency: 'EUR',
        balance: 0,
      },
    };

    expect(_setBalance('EUR', 0)).toStrictEqual(expectedAction);
  });
  it('should dispatch SET_BALANCE with history', () => {
    setBalance('EUR', 0)(mockDispatch);

    expect(mockDispatch.mock.calls[0][0]).toMatchObject({ type: SET_BALANCE });
    expect(mockDispatch.mock.calls[1][0]).toMatchObject({ type: ADD_HISTORY });
  });
  it('should create _updateBalance action', () => {
    const expectedAction = {
      type: UPDATE_BALANCE,
      payload: {
        currency: 'EUR',
        amount: 0,
      },
    };

    expect(_updateBalance('EUR', 0)).toStrictEqual(expectedAction);
  });
  it('should dispatch UPDATE_BALANCE with history', () => {
    updateBalance('EUR', 10)(mockDispatch);

    expect(mockDispatch.mock.calls[0][0].type).toEqual(UPDATE_BALANCE);
    expect(mockDispatch.mock.calls[1][0].type).toEqual(ADD_HISTORY);
    expect(mockDispatch.mock.calls[1][0].payload).toMatchObject({
      icon: 'arrow up',
      message: 'Added 10 EUR to pocket balance.',
    });
  });
  it('should dispatch UPDATE_BALANCE with history for negative value', () => {
    updateBalance('EUR', -10)(mockDispatch);

    expect(mockDispatch.mock.calls[0][0].type).toEqual(UPDATE_BALANCE);
    expect(mockDispatch.mock.calls[1][0].type).toEqual(ADD_HISTORY);
    expect(mockDispatch.mock.calls[1][0].payload).toMatchObject({
      icon: 'arrow down',
      message: 'Subtracted -10 EUR from pocket balance.',
    });
  });
  it('should dispatch exchange', () => {
    exchange({ from: ['EUR', 10], to: ['USD', 15] })(mockDispatch);

    expect(mockDispatch.mock.calls[0][0] instanceof Function).toBe(true);
    expect(mockDispatch.mock.calls[1][0] instanceof Function).toBe(true);
    expect(mockDispatch.mock.calls[2][0].type).toEqual(ADD_HISTORY);
    expect(mockDispatch.mock.calls[2][0].payload).toMatchObject({
      icon: 'exchange',
      message: 'Exchanged 10 EUR for 15 USD.',
    });
  });
});
