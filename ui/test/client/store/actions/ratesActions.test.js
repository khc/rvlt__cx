import { setRates, SET_RATES } from '../../../../src/client/store/actions/ratesActions';

describe('ratesActions', () => {
  it('should create Redux action', () => {
    const mockRates = {
      base: 'USD',
      rates: {
        EUR: 0.8,
      },
    };
    const expectedAction = {
      type: SET_RATES,
      payload: {
        base: 'USD',
        rates: {
          USD: 1,
          EUR: 0.8,
        },
      },
    };

    expect(setRates(mockRates)).toStrictEqual(expectedAction);
  });
});
