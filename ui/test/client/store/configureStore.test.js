import configureStore from '../../../src/client/store/configureStore';

describe('configureStore', () => {
  it('shoud create Redux store', () => {
    const expectedState = {
      history: [],
      pockets: [],
      rates: {
        loaded: false,
      },
    };

    const { store } = configureStore();

    expect(store).toHaveProperty('dispatch');
    expect(store.getState()).toStrictEqual(expectedState);
  });
  it('shoud create Redux store with logger', () => {
    const expectedState = {
      history: [],
      pockets: [],
      rates: {
        loaded: false,
      },
    };

    const { store } = configureStore({}, true);

    // expect(store).toHaveProperty('dispatch');
    expect(store.getState()).toStrictEqual(expectedState);
  });
});
