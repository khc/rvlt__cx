import { decimalPlaces } from '../../../src/client/utils/numberUtils';

describe('numberUtils', () => {
  it('should return 2 decimal places without argument', () => {
    const number = 3.14159265359;

    expect(decimalPlaces(number)).toEqual(3.14);
  });
  it('should return set decimal places with argument', () => {
    const number = 3.14159265359;

    expect(decimalPlaces(number, 4)).toEqual(3.1416);
  });
});
