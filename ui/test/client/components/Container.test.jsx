import React from 'react';
import { shallow } from 'enzyme';
import { GridRow, Container, SegmentGroup } from 'semantic-ui-react';
import ContainerComponent from '../../../src/client/components/Container';

describe('<Container />', () => {
  it('should render children in Semantic UI element structure', () => {
    const dummyText = 'some text';
    const wrapper = shallow(<ContainerComponent>{dummyText}</ContainerComponent>);
    expect(
      wrapper
        .find(GridRow)
        .find(Container)
        .find(SegmentGroup)
        .children()
        .text(),
    ).toEqual(dummyText);
  });
});
