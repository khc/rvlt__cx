import React from 'react';
import { shallow } from 'enzyme';
import withLog from '../../../src/client/components/withLog';

describe('withLog', () => {
  it('should log component properties and render passed component', () => {
    const dummyText = 'lorem ipsum';
    const dummyProps = {
      className: 'bold',
    };

    jest.spyOn(global.console, 'log');

    const WrappedComponent = withLog('div');
    const wrapper = shallow(<WrappedComponent {...dummyProps}>{dummyText}</WrappedComponent>);

    expect(global.console.log).toBeCalledWith({ ...dummyProps, children: dummyText });
    expect(wrapper.text()).toEqual(dummyText);

    jest.restoreAllMocks();
  });
});
