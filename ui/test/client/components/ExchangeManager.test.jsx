import React from 'react';
import { shallow } from 'enzyme';
import ExchangeManager from '../../../src/client/components/Exchange/ExchangeManager';

const lastArgs = mock => mock.mock.calls[mock.mock.calls.length - 1][0];

describe('<ExchangeManager />', () => {
  let children;
  const mockProps = {
    pockets: [
      { currency: 'EUR', balance: 100 },
      { currency: 'USD', balance: 50 },
    ],
    rates: {
      USD: 1,
      EUR: 0.5,
    },
    base: 'USD',
  };

  beforeEach(() => {
    children = jest.fn();
    mockProps.exchange = jest.fn();
  });
  afterEach(() => {
    jest.resetAllMocks();
  });
  it('should calculate pockets for dropdowns and set basic props', () => {
    const expectedProps = {
      fromPockets: [{
        key: 'EUR',
        text: 'EUR',
        value: 'EUR',
      }, {
        key: 'USD',
        text: 'USD',
        value: 'USD',
      }],
      fromPocket: null,
      fromAmount: 0,
      toPockets: [{
        key: 'EUR',
        text: 'EUR',
        value: 'EUR',
      }, {
        key: 'USD',
        text: 'USD',
        value: 'USD',
      }],
      toPocket: null,
      toAmount: 0,
      exchangeRate: null,
      reversed: false,
      canExchange: false,
    };

    shallow(<ExchangeManager {...mockProps}>{children}</ExchangeManager>);
    expect(lastArgs(children)).toMatchObject(expectedProps);
  });
  it('should set from value and calculate to value based on rate', () => {
    const expectedProps = {
      fromPocket: 'EUR',
      fromAmount: 10,
      toPocket: 'USD',
      toAmount: 20,
      canExchange: true,
      exchangeRate: 2,
    };

    shallow(<ExchangeManager {...mockProps}>{children}</ExchangeManager>);

    lastArgs(children).call('changeFromPocket')(null, { value: 'EUR' });
    lastArgs(children).call('changeToPocket')(null, { value: 'USD' });
    lastArgs(children).call('setReversed')()(null);
    lastArgs(children).call('changeFromAmount')(null, { value: 10 });

    expect(lastArgs(children)).toMatchObject(expectedProps);
  });
  it('shoud reverse the calculation', () => {
    const expectedProps = {
      fromPocket: 'EUR',
      fromAmount: 5,
      toPocket: 'USD',
      toAmount: 10,
      canExchange: true,
      exchangeRate: 2,
    };

    shallow(<ExchangeManager {...mockProps}>{children}</ExchangeManager>);

    lastArgs(children).call('changeFromPocket')(null, { value: 'EUR' });
    lastArgs(children).call('changeToPocket')(null, { value: 'USD' });
    lastArgs(children).call('setReversed')(true)(null);
    lastArgs(children).call('changeToAmount')(null, { value: 10 });

    expect(lastArgs(children)).toMatchObject(expectedProps);
  });
  it('should remove to pocket when from selected to the same value', () => {
    const expectedProps = {
      toPocket: null,
    };

    shallow(<ExchangeManager {...mockProps}>{children}</ExchangeManager>);

    lastArgs(children).call('changeFromPocket')(null, { value: 'EUR' });
    lastArgs(children).call('changeToPocket')(null, { value: 'USD' });
    lastArgs(children).call('changeFromPocket')(null, { value: 'USD' });

    expect(lastArgs(children)).toMatchObject(expectedProps);
  });
  it('should make exchange', () => {
    const expectedProps = {
      canExchange: true,
    };
    const expectedTransaction = {
      from: ['EUR', -10],
      to: ['USD', 20],
    };

    shallow(<ExchangeManager {...mockProps}>{children}</ExchangeManager>);

    lastArgs(children).call('changeFromPocket')(null, { value: 'EUR' });
    lastArgs(children).call('changeToPocket')(null, { value: 'USD' });
    lastArgs(children).call('changeFromAmount')(null, { value: 10 });

    expect(lastArgs(children)).toMatchObject(expectedProps);

    lastArgs(children).call('exchange')(null);

    expect(lastArgs(mockProps.exchange)).toMatchObject(expectedTransaction);
  });
});
