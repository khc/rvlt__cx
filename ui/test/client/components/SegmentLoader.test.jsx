import React from 'react';
import { shallow } from 'enzyme';
import { Loader } from 'semantic-ui-react';
import Segment from '../../../src/client/components/Segment';
import SegmentLoader from '../../../src/client/components/SegmentLoader';

describe('<SegmentLoader />', () => {
  it('should render Loader element', () => {
    const mockHeader = 'lorem ipsum 1';
    const mockMessage = 'lorem ipsum 2';
    const wrapper = shallow(<SegmentLoader header={mockHeader} message={mockMessage} />);

    expect(wrapper.find(Segment).prop('header')).toEqual(mockHeader);
    expect(wrapper.find(Loader).children().text()).toEqual(mockMessage);
  });
});
