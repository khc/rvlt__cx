import React from 'react';
import { mount } from 'enzyme';
import withLoader from '../../../src/client/components/withLoader';

describe('withLoader', () => {
  const MockLoader = () => 'loader';
  const MockComponent = () => 'component';

  it('should return Loader with loaded false prop', () => {
    const WrappedCompoment = withLoader(MockLoader)(MockComponent);
    const wrapper = mount(<WrappedCompoment loaded={false} />);

    expect(wrapper.find(MockLoader).exists()).toBe(true);
  });
  it('should return Component with loaded true prop', () => {
    const WrappedCompoment = withLoader(MockLoader)(MockComponent);
    const wrapper = mount(<WrappedCompoment loaded />);

    expect(wrapper.find(MockComponent).exists()).toBe(true);
  });
});
