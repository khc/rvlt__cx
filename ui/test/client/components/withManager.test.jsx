import React from 'react';
import { mount } from 'enzyme';
import Manager from '../../../src/client/components/Manager'; 
import withManager from '../../../src/client/components/withManager';

class MockManager extends Manager {
  state = {
    mockStateProp: 'lorem ipsum 1',
  }
}

describe('withManager', () => {
  it('should pass props to managed component', () => {
    const ManagedComponent = () => (<div />);

    const WrappedComponent = withManager(MockManager)(ManagedComponent);
    const wrapper = mount(<WrappedComponent mockInlineProp="lorem ipsum 2" />);

    expect(wrapper.find(ManagedComponent).prop('mockStateProp')).toEqual('lorem ipsum 1');
    expect(wrapper.find(ManagedComponent).prop('mockInlineProp')).toEqual('lorem ipsum 2');
    expect(wrapper.find(ManagedComponent).prop('call') instanceof Function).toBe(true);
  });
});