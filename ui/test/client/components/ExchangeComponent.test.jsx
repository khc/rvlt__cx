import React from 'react';
import { shallow } from 'enzyme';
import { Input, Button, Label } from 'semantic-ui-react';
import ExchangeComponent from '../../../src/client/components/Exchange/ExchangeComponent';

describe('<ExchangeComponent />', () => {
  it('should render basic view', () => {
    const mockProps = {
      fromAmount: 10,
      toAmount: 20,
      canExchange: false,
      call: jest.fn().mockReturnValue(jest.fn()),
    };
    const wrapper = shallow(<ExchangeComponent {...mockProps} />);
    const inputWrappers = wrapper.find(Input);

    expect(inputWrappers.at(0).prop('value')).toEqual(10);
    expect(inputWrappers.at(1).prop('value')).toEqual(20);
    expect(wrapper.find(Button).prop('disabled')).toEqual(true);
    expect(wrapper.find(Label).exists()).toEqual(false);
  });
  it('should show exchange rate', () => {
    const mockExchangeRate = 2;
    const mockProps = {
      fromPocket: 'EUR',
      fromAmount: 10,
      toPocket: 'USD',
      toAmount: 20,
      exchangeRate: mockExchangeRate,
      canExchange: false,
      call: jest.fn().mockReturnValue(jest.fn()),
    };

    const wrapper = shallow(<ExchangeComponent {...mockProps} />);
    const labelWrapper = wrapper.find(Label);

    expect(labelWrapper.exists()).toEqual(true);
    expect(labelWrapper.childAt(1).text()).toEqual(mockExchangeRate.toString());
  });
  it('should enable exchange button', () => {
    const mockEvent = jest.fn();
    const mockProps = {
      fromPocket: 'EUR',
      fromAmount: 10,
      toPocket: 'USD',
      toAmount: 20,
      exchangeRate: 2,
      canExchange: true,
      call: () => mockEvent,
    };

    const wrapper = shallow(<ExchangeComponent {...mockProps} />);

    expect(wrapper.find(Button).prop('disabled')).toEqual(false);
  });
  it('should call handle functions', () => {
    let mockEvents = {};
    const mockProps = {
      fromPocket: 'EUR',
      fromAmount: 10,
      toPocket: 'USD',
      toAmount: 20,
      exchangeRate: 2,
      canExchange: true,
      call: (eventName) => {
        const mockEvent = jest.fn();
        mockEvents = { ...mockEvents, [eventName]: mockEvent };
        return mockEvent;
      },
    };

    const wrapper = shallow(<ExchangeComponent {...mockProps} />);

    expect(mockEvents.exchange).toBeCalledTimes(0);
    wrapper.find(Button).simulate('click');
    expect(mockEvents.exchange).toBeCalledTimes(1);
  });
});
