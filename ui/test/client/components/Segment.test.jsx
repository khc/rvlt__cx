import React from 'react';
import { shallow } from 'enzyme';
import { Segment, Header } from 'semantic-ui-react';
import SegmentComponent from '../../../src/client/components/Segment';

describe('<Segment />', () => {
  it('should render children in Semantic UI Segment', () => {
    const dummyText = 'some text';
    const wrapper = shallow(<SegmentComponent>{dummyText}</SegmentComponent>);

    expect(wrapper.find(Segment).children().text()).toEqual(dummyText);
  });
  it('should render header when header prop is passed', () => {
    const dummyText = 'text';
    const dummyHeader = 'header';
    const wrapper = shallow(<SegmentComponent header={dummyHeader}>{dummyText}</SegmentComponent>);

    expect(wrapper.find(Segment).find(Header).children().text()).toEqual(dummyHeader);
  });
  it('should pass props to Semantic UI Segment component', () => {
    const dummyText = 'text';
    const dummyProps = {
      textAlign: 'right',
    };
    const wrapper = shallow(<SegmentComponent {...dummyProps}>{dummyText}</SegmentComponent>);

    expect(wrapper.find(Segment).props()).toMatchObject(dummyProps);
  });
});
