import React from 'react';
import { mount } from 'enzyme';
import { Card } from 'semantic-ui-react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import Pockets from '../../../src/client/components/Pockets';

describe('<History />', () => {
  const mockEntries = [{
    currency: 'EUR',
    balance: 10,
  }, {
    currency: 'USD',
    balance: 20,
  }];
  it('should render history', () => {
    const mockStore = createStore(combineReducers({ pockets: () => mockEntries }));
    const wrapper = mount(<Provider store={mockStore}><Pockets /></Provider>);

    expect(wrapper.find(Card).length).toEqual(2);
    expect(wrapper.find(Card).at(0).find(Card.Description).text()).toEqual('10');
    expect(wrapper.find(Card).at(1).find(Card.Description).text()).toEqual('20');
  });
});
