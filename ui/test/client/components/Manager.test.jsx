import React from 'react';
import { mount } from 'enzyme';
import Manager from '../../../src/client/components/Manager';

const lastArgs = mock => mock.mock.calls[mock.mock.calls.length - 1][0];

describe('<Manager />', () => {
  let mockChildren;

  beforeEach(() => {
    mockChildren = jest.fn().mockReturnValue(null);
  });
  afterEach(() => {
    jest.restoreAllMocks();
  });
  it('should render children as a function and pass call method', () => {
    mount(<Manager>{mockChildren}</Manager>);

    expect(lastArgs(mockChildren).call instanceof Function).toBe(true);
  });
  it('should throw an error when called not existing method', () => {
    mount(<Manager>{mockChildren}</Manager>);

    expect(lastArgs(mockChildren).call('method')).toThrow('Method \'method\' does not exists in manager.');
  });
});
