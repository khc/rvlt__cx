import React from 'react';
import { mount } from 'enzyme';
import { List } from 'semantic-ui-react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import History from '../../../src/client/components/History';

describe('<History />', () => {
  const mockEntries = [{
    message: 'lorem ipsum 1',
    icon: 'plus',
  }, {
    message: 'lorem ipsum 2',
    icon: 'minus',
  }];
  it('should render history', () => {
    const mockStore = createStore(combineReducers({ history: () => mockEntries }));
    const wrapper = mount(<Provider store={mockStore}><History /></Provider>);

    expect(wrapper.find(List.Item).length).toEqual(2);
    expect(wrapper.find(List.Item).at(0).find(List.Content).text()).toEqual(mockEntries[0].message);
    expect(wrapper.find(List.Item).at(1).find(List.Content).text()).toEqual(mockEntries[1].message);
  });
});
