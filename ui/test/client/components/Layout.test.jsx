import React from 'react';
import { shallow } from 'enzyme';
import { GridRow, Container, Header } from 'semantic-ui-react';
import LayoutComponent from '../../../src/client/components/Layout';
import { APP_TITLE, APP_DESCRIPTION } from '../../../src/client/constants';

describe('<Layout />', () => {
  it('should render application header and children', () => {
    const dummyText = 'some text';
    const wrapper = shallow(<LayoutComponent>{dummyText}</LayoutComponent>);

    const headerWrapper = wrapper
      .find(GridRow)
      .find(Container)
      .find(Header);

    expect(
      wrapper
        .childAt(1)
        .text(),
    ).toEqual(dummyText);

    expect(
      headerWrapper
        .childAt(0)
        .text(),
    ).toEqual(APP_TITLE);

    expect(
      headerWrapper
        .find(Header.Subheader)
        .childAt(0)
        .text(),
    ).toEqual(APP_DESCRIPTION);
  });
});
