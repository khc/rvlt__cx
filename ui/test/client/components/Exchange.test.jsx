import React from 'react';
import { mount } from 'enzyme';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import Exchange from '../../../src/client/components/Exchange';
import SegmentLoader from '../../../src/client/components/SegmentLoader';

describe('<Exchange />', () => {
  it('should render Loader if rates are not available', () => {
    const mockStore = createStore(combineReducers({ rates: () => ({ loaded: false }) }));
    mockStore.dispatch = jest.fn();

    const wrapper = mount(<Provider store={mockStore}><Exchange /></Provider>);

    wrapper.childAt(0).childAt(0).prop('exchange')('transaction');
    expect(mockStore.dispatch).toBeCalled();
    expect(wrapper.find(SegmentLoader).exists()).toBe(true);
  });
});
