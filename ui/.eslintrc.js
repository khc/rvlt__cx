module.exports = {
  extends: 'airbnb',
  parser: 'babel-eslint',
  env: {
    browser: true
  },
  rules: {
    'no-underscore-dangle': 0,
    'import/no-extraneous-dependencies': [
      'error', {
        devDependencies: true,
      }
    ],
    'react/destructuring-assignment': [
      'error',
      'always', {
        ignoreClassFields: true
      }
    ]
  }
}