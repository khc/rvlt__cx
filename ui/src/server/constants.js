/* eslint-disable import/prefer-default-export */

export const {
  APP_PORT = 3000,
  API_BASEPATH,
} = process.env;
