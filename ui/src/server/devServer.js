import express from 'express';
import morgan from 'morgan';
import { resolve } from 'path';
import Bundler from 'parcel-bundler';
import { APP_PORT, API_BASEPATH } from './constants';

const app = express();

app.set('view engine', 'pug');
app.set('views', resolve(__dirname, './views'));

const bundler = new Bundler(resolve(__dirname, '../client/index.jsx'), {
  outDir: resolve(__dirname, '../../public'),
});

app.use(morgan('dev'));
app.use(bundler.middleware());

app.get('*', (req, res) => {
  res.render('index', {
    CONFIG: JSON.stringify({ API_BASEPATH }),
  });
});

app.listen(APP_PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`Listening on port ${APP_PORT}.`);
});
