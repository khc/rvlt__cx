import { createStore, combineReducers, applyMiddleware } from 'redux';
import promiseMiddleware from 'redux-promise';
import thunkMiddleware from 'redux-thunk';
import loggerMiddleware from 'redux-logger';

import rates from './reducers/ratesReducer';
import pockets from './reducers/pocketsReducer';
import history from './reducers/historyReducer';

const configureStore = (initialState = {}, enableLogger = false) => {
  const middleware = [
    promiseMiddleware,
    thunkMiddleware,
    ...(enableLogger ? [loggerMiddleware] : []),
  ];

  const store = createStore(
    combineReducers({
      pockets,
      rates,
      history,
    }),
    initialState,
    applyMiddleware(...middleware),
  );

  return { store };
};

export default configureStore;
