import { handleActions } from 'redux-actions';
import { SET_RATES } from '../actions/ratesActions';

const ratesReducer = handleActions({
  [SET_RATES]: {
    next: (state, action) => ({
      ...action.payload,
      loaded: true,
    }),
  },
}, {
  loaded: false,
});

export default ratesReducer;
