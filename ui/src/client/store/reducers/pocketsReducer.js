import { handleActions } from 'redux-actions';
import { ADD_POCKET, SET_BALANCE, UPDATE_BALANCE } from '../actions/pocketsActions';

const pocketsReducer = handleActions({
  [ADD_POCKET]: {
    next: (state, action) => [
      ...state,
      action.payload,
    ],
  },
  [SET_BALANCE]: {
    next: (state, action) => state.map(({ currency, balance }) => ({
      currency,
      balance: currency === action.payload.currency ? action.payload.balance : balance,
    })),
  },
  [UPDATE_BALANCE]: {
    next: (state, action) => state.map(({ currency, balance }) => ({
      currency,
      balance: currency === action.payload.currency ? (balance + action.payload.amount) : balance,
    })),
  },
}, []);

export default pocketsReducer;
