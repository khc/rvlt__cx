import { handleActions } from 'redux-actions';
import { ADD_HISTORY } from '../actions/historyActions';

const historyReducer = handleActions({
  [ADD_HISTORY]: {
    next: (state, action) => [
      action.payload,
      ...state,
    ],
  },
}, []);

export default historyReducer;
