/* eslint-disable import/prefer-default-export */
import { createAction } from 'redux-actions';

export const ADD_HISTORY = 'ADD_HISTORY';
export const addHistory = createAction(ADD_HISTORY, (message, icon = '') => ({
  message,
  icon,
  timestamp: new Date(),
}));
