/* eslint-disable import/prefer-default-export */
import { createAction } from 'redux-actions';
import { addHistory } from './historyActions';

export const ADD_POCKET = 'ADD_POCKET';
export const _addPocket = createAction(ADD_POCKET, (currency, balance = 0) => ({
  currency,
  balance,
}));

export const addPocket = (currency, balance = 0) => (dispatch) => {
  dispatch(_addPocket(currency, balance));
  dispatch(addHistory(`Added ${currency} pocket with initial balance of ${balance}.`, 'add'));
};

export const SET_BALANCE = 'SET_BALANCE';
export const _setBalance = createAction(SET_BALANCE, (currency, balance) => ({
  currency,
  balance,
}));

export const setBalance = (currency, balance) => (dispatch) => {
  dispatch(_setBalance(currency, balance));
  dispatch(addHistory(`Set pocket balance to ${balance} ${currency}.`, 'sync alternate'));
};

export const UPDATE_BALANCE = 'UPDATE_BALANCE';
export const _updateBalance = createAction(UPDATE_BALANCE, (currency, amount) => ({
  currency,
  amount,
}));

export const updateBalance = (currency, amount) => (dispatch) => {
  const icon = amount > 0 ? 'arrow up' : 'arrow down';
  const operation = amount > 0 ? 'Added' : 'Subtracted';
  const preposition = amount > 0 ? 'to' : 'from';


  dispatch(_updateBalance(currency, amount));
  dispatch(addHistory(`${operation} ${amount} ${currency} ${preposition} pocket balance.`, icon));
};

export const exchange = transaction => (dispatch) => {
  dispatch(updateBalance(...transaction.from));
  dispatch(updateBalance(...transaction.to));
  dispatch(addHistory(`Exchanged ${transaction.from[1]} ${transaction.from[0]} for ${transaction.to[1]} ${transaction.to[0]}.`, 'exchange'));
};
