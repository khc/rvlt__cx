/* eslint-disable import/prefer-default-export */
import { createAction } from 'redux-actions';

export const SET_RATES = 'SET_RATES';
export const setRates = createAction(SET_RATES, ({ base, rates }) => ({
  base,
  rates: {
    [base]: 1,
    ...rates,
  },
}));
