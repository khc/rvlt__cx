import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { Grid } from 'semantic-ui-react';
import configureStore from './store/configureStore';
import { setRates } from './store/actions/ratesActions';
import { addPocket } from './store/actions/pocketsActions';
import Layout from './components/Layout';
import Pockets from './components/Pockets';
import Exchange from './components/Exchange';
import History from './components/History';

const { store } = configureStore();
const source = new EventSource(window.env.API_BASEPATH);

store.dispatch(addPocket('EUR', 100));
store.dispatch(addPocket('USD', 50));
store.dispatch(addPocket('GBP'));

source.addEventListener('rates', ({ data }) => {
  store.dispatch(setRates(JSON.parse(data)));
});

const Application = () => (
  <ReduxProvider store={store}>
    <Grid padded>
      <Layout>
        <Pockets />
        <Exchange />
        <History />
      </Layout>
    </Grid>
  </ReduxProvider>
);

export default Application;
