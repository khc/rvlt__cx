/* eslint-disable import/prefer-default-export */
import PropTypes from 'prop-types';

const pocket = PropTypes.shape({
  currency: PropTypes.string.isRequired,
  balance: PropTypes.number.isRequired,
});

const pockets = PropTypes.arrayOf(
  pocket,
);

export default {
  ...PropTypes,
  pocket,
  pockets,
};
