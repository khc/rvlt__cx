import React from 'react';

const withLog = WrappedComponent => (props) => {
  // eslint-disable-next-line no-console
  console.log(props);
  return <WrappedComponent {...props} />;
};

export default withLog;
