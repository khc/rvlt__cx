import React from 'react';
import { connect } from 'react-redux';
import { Card } from 'semantic-ui-react';
import { generate } from 'shortid';
import Container from './Container';
import Segment from './Segment';
import { decimalPlaces } from '../utils/numberUtils';
import PropTypes from '../propTypes';

const Pockets = ({ pockets }) => (
  <Container>
    <Segment header="Pockets">
      <Card.Group itemsPerRow={4}>
        {
          pockets.map(pocket => (
            <Card key={generate()} className="pocket">
              <Card.Content>
                <Card.Header>{pocket.currency}</Card.Header>
                <Card.Description>
                  {decimalPlaces(pocket.balance)}
                </Card.Description>
              </Card.Content>
            </Card>
          ))
        }
      </Card.Group>
    </Segment>
  </Container>

);

Pockets.propTypes = {
  pockets: PropTypes.pockets.isRequired,
};

const mapStateToProps = ({ pockets }) => ({
  pockets,
});

export default connect(mapStateToProps)(Pockets);
