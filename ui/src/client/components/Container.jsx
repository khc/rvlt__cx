import React from 'react';
import { Grid, Container, Segment } from 'semantic-ui-react';
import PropTypes from '../propTypes';

const ContainerComponent = ({ children }) => (
  <Grid.Row>
    <Container>
      <Segment.Group>
        {children}
      </Segment.Group>
    </Container>
  </Grid.Row>
);

ContainerComponent.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ContainerComponent;
