import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { GridRow, Container, Header } from 'semantic-ui-react';
import { APP_TITLE, APP_DESCRIPTION } from '../constants';

const Layout = ({ children }) => (
  <Fragment>
    <GridRow>
      <Container>
        <Header as="h1">
          {APP_TITLE}
          <Header.Subheader>
            {APP_DESCRIPTION}
          </Header.Subheader>
        </Header>
      </Container>
    </GridRow>
    { children }
  </Fragment>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
