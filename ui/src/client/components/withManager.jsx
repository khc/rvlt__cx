import React from 'react';

// eslint-disable-next-line react/prop-types
const withManager = ManagerComponent => ManagedComponenent => ({ children, ...props }) => (
  <ManagerComponent {...props}>
    {
      managerProps => <ManagedComponenent {...managerProps}>{children}</ManagedComponenent>
    }
  </ManagerComponent>
);

export default withManager;
