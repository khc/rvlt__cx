import { compose } from 'redux';
import { connect } from 'react-redux';
import ExchangeComponent from './Exchange/ExchangeComponent';
import ExchangeManager from './Exchange/ExchangeManager';
import SegmentLoader from './SegmentLoader';
import withLoader from './withLoader';
import withManager from './withManager';
import { exchange } from '../store/actions/pocketsActions';

const mapStateToProps = ({ pockets, rates: { base, rates, loaded } }) => ({
  pockets,
  base,
  rates,
  loaded,
});

const mapDispatchToProps = dispatch => ({
  exchange: (transaction) => {
    dispatch(exchange(transaction));
  },
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withLoader(SegmentLoader, { header: 'Exchange', message: 'Waiting for rates.' }),
  withManager(ExchangeManager),
)(ExchangeComponent);
