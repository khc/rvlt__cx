import React from 'react';
import {
  Loader,
} from 'semantic-ui-react';
import Container from './Container';
import Segment from './Segment';
import PropTypes from '../propTypes';

const SegmentLoader = ({ header, message }) => (
  <Container>
    <Segment header={header}>
      <Loader active inline="centered">{message}</Loader>
    </Segment>
  </Container>
);

SegmentLoader.propTypes = {
  header: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
};

export default SegmentLoader;
