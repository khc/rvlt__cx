import React from 'react';
import { connect } from 'react-redux';
import { generate } from 'shortid';
import { List } from 'semantic-ui-react';
import Container from './Container';
import Segment from './Segment';
import PropTypes from '../propTypes';

export const HistoryComponent = ({ history }) => (
  <Container>
    <Segment header="History">
      <List>
        {
          history.map(({ message, icon }) => (
            <List.Item key={generate()}>
              <List.Icon name={icon} />
              <List.Content>
                {message}
              </List.Content>
            </List.Item>
          ))
        }
      </List>
    </Segment>
  </Container>
);

HistoryComponent.propTypes = {
  history: PropTypes.arrayOf(
    PropTypes.shape(),
  ).isRequired,
};

const mapStateToProps = ({ history }) => ({
  history,
});

export default connect(mapStateToProps)(HistoryComponent);
