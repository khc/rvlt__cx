import React from 'react';
import {
  Grid,
  Dropdown,
  Input,
  Button,
  Label,
  Icon,
} from 'semantic-ui-react';

import Container from '../Container';
import Segment from '../Segment';
import PropTypes from '../../propTypes';
import { decimalPlaces } from '../../utils/numberUtils';

const ExchangeComponent = ({
  fromPocket,
  fromPockets,
  fromAmount,
  toPocket,
  toPockets,
  toAmount,
  exchangeRate,
  canExchange,
  call,
}) => (
  <Container>
    <Segment header="Exchange">
      <Grid columns={3}>
        <Grid.Row>
          <Grid.Column>
            <Dropdown className="block" placeholder="Exchange from..." onChange={call('changeFromPocket')} options={fromPockets} />
            <Input
              type="number"
              transparent
              size="massive"
              value={decimalPlaces(fromAmount)}
              onFocus={call('setReversed')(false)}
              onChange={call('changeFromAmount')}
            />
          </Grid.Column>
          <Grid.Column textAlign="center" verticalAlign="middle">
            {
              exchangeRate > 0 && (
                <Label color="red">
                  <Icon name="chart line" />
                  {decimalPlaces(exchangeRate, 4)}
                </Label>
              )
            }
          </Grid.Column>
          <Grid.Column textAlign="right">
            <div>
              <Dropdown placeholder="Exchange to..." onChange={call('changeToPocket')} value={toPocket} options={toPockets} disabled={!fromPocket} />
            </div>
            <Input
              type="number"
              transparent
              size="massive"
              className="right"
              value={decimalPlaces(toAmount)}
              onFocus={call('setReversed')(true)}
              onChange={call('changeToAmount')}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
    <Segment textAlign="right">
      <Button color="blue" disabled={!canExchange} onClick={call('exchange')}>Exchange</Button>
    </Segment>

  </Container>
);

ExchangeComponent.propTypes = {
  fromPocket: PropTypes.string,
  fromPockets: PropTypes.arrayOf(
    PropTypes.shape(),
  ),
  fromAmount: PropTypes.number.isRequired,
  toPocket: PropTypes.string,
  toPockets: PropTypes.arrayOf(
    PropTypes.shape(),
  ),
  toAmount: PropTypes.number.isRequired,
  exchangeRate: PropTypes.number,
  canExchange: PropTypes.bool.isRequired,
  call: PropTypes.func.isRequired,
};

ExchangeComponent.defaultProps = {
  fromPocket: '',
  fromPockets: [],
  toPocket: '',
  toPockets: [],
  exchangeRate: 0,
};

export default ExchangeComponent;
