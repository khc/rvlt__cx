import Manager from '../Manager';

class ExchangeManager extends Manager {
  state = {
    fromPocket: null,
    fromAmount: 0,
    toPocket: null,
    toAmount: 0,
    exchangeRate: null,
    reversed: false,
    canExchange: false,
  };

  static getDerivedStateFromProps({ pockets, rates, base }, state) {
    const {
      fromAmount,
      fromPocket,
      toAmount,
      toPocket,
      reversed,
    } = state;

    const exchangeRate = (
      (fromPocket && toPocket) ? rates[base] / rates[fromPocket] * rates[toPocket] : null
    );

    // eslint-disable-next-line no-nested-ternary
    const amount = exchangeRate
      ? ((reversed)
        ? { fromAmount: toAmount / exchangeRate }
        : { toAmount: fromAmount * exchangeRate }
      )
      : {};

    const canExchange = !!exchangeRate && pockets
      .some(({ currency, balance }) => currency === fromPocket && balance > fromAmount);

    return ({
      ...state,
      ...amount,
      exchangeRate,
      canExchange,
    });
  }

  changeFromPocket = (e, { value }) => {
    this.setState(state => ({
      fromPocket: value,
      toPocket: state.toPocket === value ? null : state.toPocket,
    }));
  }

  changeFromAmount = (e, { value }) => {
    this.setState({ fromAmount: parseFloat(value, 10) });
  }

  changeToPocket = (e, { value }) => {
    this.setState({
      toPocket: value,
    });
  }

  setReversed = (reversed = false) => () => {
    this.setState({ reversed });
  }

  changeToAmount = (e, { value }) => {
    this.setState({ toAmount: parseFloat(value, 10) });
  }

  exchange = () => {
    const {
      fromPocket,
      fromAmount,
      toPocket,
      toAmount,
    } = this.state;
    this.props.exchange({
      from: [fromPocket, -parseFloat(fromAmount, 10)],
      to: [toPocket, parseFloat(toAmount, 10)],
    });
  }

  fromPockets = () => {
    const { pockets } = this.props;

    return pockets
      .filter(({ balance }) => balance > 0)
      .map(({ currency }) => ({ key: currency, value: currency, text: currency }));
  }

  toPockets = () => {
    const { pockets } = this.props;
    const { fromPocket } = this.state;

    return pockets
      .filter(({ currency }) => currency !== fromPocket)
      .map(({ currency }) => ({ key: currency, value: currency, text: currency }));
  }

  addProps = () => ({
    fromPockets: this.fromPockets(),
    toPockets: this.toPockets(),
  })
}

export default ExchangeManager;
