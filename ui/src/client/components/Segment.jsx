import React from 'react';
import PropTypes from 'prop-types';
import {
  Segment,
  Header,
} from 'semantic-ui-react';

const SegmentComponent = ({ header, children, ...props }) => (
  <Segment {...props}>
    { header && <Header>{header}</Header> }
    {children}
  </Segment>
);

SegmentComponent.propTypes = {
  header: PropTypes.string,
  children: PropTypes.node.isRequired,
};

SegmentComponent.defaultProps = {
  header: '',
};

export default SegmentComponent;
