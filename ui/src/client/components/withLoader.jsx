import React from 'react';

const withLoader = (LoaderComponent, { checkProp = 'loaded', ...options } = {}) => WrappedComponent => ({ [checkProp]: loaded, ...props }) => (
  loaded ? <WrappedComponent loaded={loaded} {...props} /> : <LoaderComponent {...options} />
);

export default withLoader;
