import { Component } from 'react';
import PropTypes from '../propTypes';

class Manager extends Component {
  state = {
  };

  call = (methodName, options) => (event, props) => (
    this[methodName]
      ? this[methodName].call(this, event, props, options)
      : (() => {
        throw new Error(`Method '${methodName}' does not exists in manager.`);
      })()
  )

  addProps = () => ({})

  render = () => this.props.children({
    ...this.props,
    ...this.state,
    ...this.addProps(),
    call: this.call,
  });
}

Manager.propTypes = {
  children: PropTypes.func.isRequired,
};

export default Manager;
