/* eslint-disable import/prefer-default-export */

export const decimalPlaces = (number, places = 2) => (
  Math.round(number * (10 ** places)) / (10 ** places)
);
