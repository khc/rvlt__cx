up:
	@docker-compose -p rvlt__cx up

down:
	@docker-compose -p rvlt__cx down

build:
	@docker build -t rvlt__cx_api:1 ./api
	@docker build -t rvlt__cx_emitter:1 ./emitter
	@docker build -t rvlt__cx_ui:1 ./ui

.PHONY: up down build