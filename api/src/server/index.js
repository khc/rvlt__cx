import express from 'express';
import morgan from 'morgan';
import api from './routes/api/api';

const appPort = process.env.APP_PORT || 3000;
const app = express();

app.use([
  morgan('dev'),
  api(),
]);

app.listen(appPort || 3000, () => {
  // eslint-disable-next-line no-console
  console.log(`Listening on port ${appPort}.`);
});
