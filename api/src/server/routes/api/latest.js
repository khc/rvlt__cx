import { Router } from 'express';
import oxrResponse from './oxrResponse';

const router = Router();

const randomise = (rate) => {
  const min = rate * 0.99;
  const max = rate * 1.01;
  return min + (Math.random() * (max - min));
};

const rates = () => Object
  .keys(oxrResponse.rates)
  .reduce((accumulator, currency) => ({
    ...accumulator,
    [currency]: randomise(oxrResponse.rates[currency]),
  }), {});

export default () => {
  router.get('/latest.json', (req, res) => {
    res.send({
      base: 'USD',
      rates: rates(),
    });
  });

  return router;
};
