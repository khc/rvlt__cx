import { Router } from 'express';
import latest from './latest';

export default () => {
  const router = Router();

  router.use('/api', [
    latest(),
  ]);

  return router;
};
